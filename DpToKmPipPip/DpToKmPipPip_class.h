//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct  4 14:47:54 2016 by ROOT version 6.06/01
// from TTree DecayTree/DecayTree
// found on file: TeslaTuples_Sept07A_0x11381609_00_10.root
//////////////////////////////////////////////////////////

#ifndef DpToKmPipPip_class_h
#define DpToKmPipPip_class_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class DpToKmPipPip_class {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxDplus_ENDVERTEX_COV = 1;
   const Int_t kMaxDplus_OWNPV_COV = 1;
   const Int_t kMaxKminus_OWNPV_COV = 1;
   const Int_t kMaxKminus_ORIVX_COV = 1;
   const Int_t kMaxPiplus1_OWNPV_COV = 1;
   const Int_t kMaxPiplus1_ORIVX_COV = 1;
   const Int_t kMaxPiplus2_OWNPV_COV = 1;
   const Int_t kMaxPiplus2_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        Dplus_ENDVERTEX_X;
   Double_t        Dplus_ENDVERTEX_Y;
   Double_t        Dplus_ENDVERTEX_Z;
   Double_t        Dplus_ENDVERTEX_XERR;
   Double_t        Dplus_ENDVERTEX_YERR;
   Double_t        Dplus_ENDVERTEX_ZERR;
   Double_t        Dplus_ENDVERTEX_CHI2;
   Int_t           Dplus_ENDVERTEX_NDOF;
   Float_t         Dplus_ENDVERTEX_COV_[3][3];
   Double_t        Dplus_OWNPV_X;
   Double_t        Dplus_OWNPV_Y;
   Double_t        Dplus_OWNPV_Z;
   Double_t        Dplus_OWNPV_XERR;
   Double_t        Dplus_OWNPV_YERR;
   Double_t        Dplus_OWNPV_ZERR;
   Double_t        Dplus_OWNPV_CHI2;
   Int_t           Dplus_OWNPV_NDOF;
   Float_t         Dplus_OWNPV_COV_[3][3];
   Double_t        Dplus_IP_OWNPV;
   Double_t        Dplus_IPCHI2_OWNPV;
   Double_t        Dplus_FD_OWNPV;
   Double_t        Dplus_FDCHI2_OWNPV;
   Double_t        Dplus_DIRA_OWNPV;
   Double_t        Dplus_P;
   Double_t        Dplus_PT;
   Double_t        Dplus_PE;
   Double_t        Dplus_PX;
   Double_t        Dplus_PY;
   Double_t        Dplus_PZ;
   Double_t        Dplus_MM;
   Double_t        Dplus_MMERR;
   Double_t        Dplus_M;
   Int_t           Dplus_ID;
   Double_t        Dplus_TAU;
   Double_t        Dplus_TAUERR;
   Double_t        Dplus_TAUCHI2;
   Double_t        Dplus_X;
   Double_t        Dplus_Y;
   Double_t        Kminus_MC12TuneV2_ProbNNe;
   Double_t        Kminus_MC12TuneV2_ProbNNmu;
   Double_t        Kminus_MC12TuneV2_ProbNNpi;
   Double_t        Kminus_MC12TuneV2_ProbNNk;
   Double_t        Kminus_MC12TuneV2_ProbNNp;
   Double_t        Kminus_MC12TuneV2_ProbNNghost;
   Double_t        Kminus_MC12TuneV3_ProbNNe;
   Double_t        Kminus_MC12TuneV3_ProbNNmu;
   Double_t        Kminus_MC12TuneV3_ProbNNpi;
   Double_t        Kminus_MC12TuneV3_ProbNNk;
   Double_t        Kminus_MC12TuneV3_ProbNNp;
   Double_t        Kminus_MC12TuneV3_ProbNNghost;
   Double_t        Kminus_MC12TuneV4_ProbNNe;
   Double_t        Kminus_MC12TuneV4_ProbNNmu;
   Double_t        Kminus_MC12TuneV4_ProbNNpi;
   Double_t        Kminus_MC12TuneV4_ProbNNk;
   Double_t        Kminus_MC12TuneV4_ProbNNp;
   Double_t        Kminus_MC12TuneV4_ProbNNghost;
   Double_t        Kminus_MC15TuneV1_ProbNNe;
   Double_t        Kminus_MC15TuneV1_ProbNNmu;
   Double_t        Kminus_MC15TuneV1_ProbNNpi;
   Double_t        Kminus_MC15TuneV1_ProbNNk;
   Double_t        Kminus_MC15TuneV1_ProbNNp;
   Double_t        Kminus_MC15TuneV1_ProbNNghost;
   Double_t        Kminus_OWNPV_X;
   Double_t        Kminus_OWNPV_Y;
   Double_t        Kminus_OWNPV_Z;
   Double_t        Kminus_OWNPV_XERR;
   Double_t        Kminus_OWNPV_YERR;
   Double_t        Kminus_OWNPV_ZERR;
   Double_t        Kminus_OWNPV_CHI2;
   Int_t           Kminus_OWNPV_NDOF;
   Float_t         Kminus_OWNPV_COV_[3][3];
   Double_t        Kminus_IP_OWNPV;
   Double_t        Kminus_IPCHI2_OWNPV;
   Double_t        Kminus_ORIVX_X;
   Double_t        Kminus_ORIVX_Y;
   Double_t        Kminus_ORIVX_Z;
   Double_t        Kminus_ORIVX_XERR;
   Double_t        Kminus_ORIVX_YERR;
   Double_t        Kminus_ORIVX_ZERR;
   Double_t        Kminus_ORIVX_CHI2;
   Int_t           Kminus_ORIVX_NDOF;
   Float_t         Kminus_ORIVX_COV_[3][3];
   Double_t        Kminus_P;
   Double_t        Kminus_PT;
   Double_t        Kminus_PE;
   Double_t        Kminus_PX;
   Double_t        Kminus_PY;
   Double_t        Kminus_PZ;
   Double_t        Kminus_M;
   Int_t           Kminus_ID;
   Double_t        Kminus_PIDe;
   Double_t        Kminus_PIDmu;
   Double_t        Kminus_PIDK;
   Double_t        Kminus_PIDp;
   Double_t        Kminus_ProbNNe;
   Double_t        Kminus_ProbNNk;
   Double_t        Kminus_ProbNNp;
   Double_t        Kminus_ProbNNpi;
   Double_t        Kminus_ProbNNmu;
   Double_t        Kminus_ProbNNghost;
   Bool_t          Kminus_hasMuon;
   Bool_t          Kminus_isMuon;
   Bool_t          Kminus_hasRich;
   Bool_t          Kminus_hasCalo;
   Int_t           Kminus_TRACK_Type;
   Int_t           Kminus_TRACK_Key;
   Double_t        Kminus_TRACK_CHI2NDOF;
   Double_t        Kminus_TRACK_PCHI2;
   Double_t        Kminus_TRACK_MatchCHI2;
   Double_t        Kminus_TRACK_GhostProb;
   Double_t        Kminus_TRACK_CloneDist;
   Double_t        Kminus_TRACK_Likelihood;
   Double_t        Kminus_X;
   Double_t        Kminus_Y;
   Double_t        Piplus1_MC12TuneV2_ProbNNe;
   Double_t        Piplus1_MC12TuneV2_ProbNNmu;
   Double_t        Piplus1_MC12TuneV2_ProbNNpi;
   Double_t        Piplus1_MC12TuneV2_ProbNNk;
   Double_t        Piplus1_MC12TuneV2_ProbNNp;
   Double_t        Piplus1_MC12TuneV2_ProbNNghost;
   Double_t        Piplus1_MC12TuneV3_ProbNNe;
   Double_t        Piplus1_MC12TuneV3_ProbNNmu;
   Double_t        Piplus1_MC12TuneV3_ProbNNpi;
   Double_t        Piplus1_MC12TuneV3_ProbNNk;
   Double_t        Piplus1_MC12TuneV3_ProbNNp;
   Double_t        Piplus1_MC12TuneV3_ProbNNghost;
   Double_t        Piplus1_MC12TuneV4_ProbNNe;
   Double_t        Piplus1_MC12TuneV4_ProbNNmu;
   Double_t        Piplus1_MC12TuneV4_ProbNNpi;
   Double_t        Piplus1_MC12TuneV4_ProbNNk;
   Double_t        Piplus1_MC12TuneV4_ProbNNp;
   Double_t        Piplus1_MC12TuneV4_ProbNNghost;
   Double_t        Piplus1_MC15TuneV1_ProbNNe;
   Double_t        Piplus1_MC15TuneV1_ProbNNmu;
   Double_t        Piplus1_MC15TuneV1_ProbNNpi;
   Double_t        Piplus1_MC15TuneV1_ProbNNk;
   Double_t        Piplus1_MC15TuneV1_ProbNNp;
   Double_t        Piplus1_MC15TuneV1_ProbNNghost;
   Double_t        Piplus1_OWNPV_X;
   Double_t        Piplus1_OWNPV_Y;
   Double_t        Piplus1_OWNPV_Z;
   Double_t        Piplus1_OWNPV_XERR;
   Double_t        Piplus1_OWNPV_YERR;
   Double_t        Piplus1_OWNPV_ZERR;
   Double_t        Piplus1_OWNPV_CHI2;
   Int_t           Piplus1_OWNPV_NDOF;
   Float_t         Piplus1_OWNPV_COV_[3][3];
   Double_t        Piplus1_IP_OWNPV;
   Double_t        Piplus1_IPCHI2_OWNPV;
   Double_t        Piplus1_ORIVX_X;
   Double_t        Piplus1_ORIVX_Y;
   Double_t        Piplus1_ORIVX_Z;
   Double_t        Piplus1_ORIVX_XERR;
   Double_t        Piplus1_ORIVX_YERR;
   Double_t        Piplus1_ORIVX_ZERR;
   Double_t        Piplus1_ORIVX_CHI2;
   Int_t           Piplus1_ORIVX_NDOF;
   Float_t         Piplus1_ORIVX_COV_[3][3];
   Double_t        Piplus1_P;
   Double_t        Piplus1_PT;
   Double_t        Piplus1_PE;
   Double_t        Piplus1_PX;
   Double_t        Piplus1_PY;
   Double_t        Piplus1_PZ;
   Double_t        Piplus1_M;
   Int_t           Piplus1_ID;
   Double_t        Piplus1_PIDe;
   Double_t        Piplus1_PIDmu;
   Double_t        Piplus1_PIDK;
   Double_t        Piplus1_PIDp;
   Double_t        Piplus1_ProbNNe;
   Double_t        Piplus1_ProbNNk;
   Double_t        Piplus1_ProbNNp;
   Double_t        Piplus1_ProbNNpi;
   Double_t        Piplus1_ProbNNmu;
   Double_t        Piplus1_ProbNNghost;
   Bool_t          Piplus1_hasMuon;
   Bool_t          Piplus1_isMuon;
   Bool_t          Piplus1_hasRich;
   Bool_t          Piplus1_hasCalo;
   Int_t           Piplus1_TRACK_Type;
   Int_t           Piplus1_TRACK_Key;
   Double_t        Piplus1_TRACK_CHI2NDOF;
   Double_t        Piplus1_TRACK_PCHI2;
   Double_t        Piplus1_TRACK_MatchCHI2;
   Double_t        Piplus1_TRACK_GhostProb;
   Double_t        Piplus1_TRACK_CloneDist;
   Double_t        Piplus1_TRACK_Likelihood;
   Double_t        Piplus1_X;
   Double_t        Piplus1_Y;
   Double_t        Piplus2_MC12TuneV2_ProbNNe;
   Double_t        Piplus2_MC12TuneV2_ProbNNmu;
   Double_t        Piplus2_MC12TuneV2_ProbNNpi;
   Double_t        Piplus2_MC12TuneV2_ProbNNk;
   Double_t        Piplus2_MC12TuneV2_ProbNNp;
   Double_t        Piplus2_MC12TuneV2_ProbNNghost;
   Double_t        Piplus2_MC12TuneV3_ProbNNe;
   Double_t        Piplus2_MC12TuneV3_ProbNNmu;
   Double_t        Piplus2_MC12TuneV3_ProbNNpi;
   Double_t        Piplus2_MC12TuneV3_ProbNNk;
   Double_t        Piplus2_MC12TuneV3_ProbNNp;
   Double_t        Piplus2_MC12TuneV3_ProbNNghost;
   Double_t        Piplus2_MC12TuneV4_ProbNNe;
   Double_t        Piplus2_MC12TuneV4_ProbNNmu;
   Double_t        Piplus2_MC12TuneV4_ProbNNpi;
   Double_t        Piplus2_MC12TuneV4_ProbNNk;
   Double_t        Piplus2_MC12TuneV4_ProbNNp;
   Double_t        Piplus2_MC12TuneV4_ProbNNghost;
   Double_t        Piplus2_MC15TuneV1_ProbNNe;
   Double_t        Piplus2_MC15TuneV1_ProbNNmu;
   Double_t        Piplus2_MC15TuneV1_ProbNNpi;
   Double_t        Piplus2_MC15TuneV1_ProbNNk;
   Double_t        Piplus2_MC15TuneV1_ProbNNp;
   Double_t        Piplus2_MC15TuneV1_ProbNNghost;
   Double_t        Piplus2_OWNPV_X;
   Double_t        Piplus2_OWNPV_Y;
   Double_t        Piplus2_OWNPV_Z;
   Double_t        Piplus2_OWNPV_XERR;
   Double_t        Piplus2_OWNPV_YERR;
   Double_t        Piplus2_OWNPV_ZERR;
   Double_t        Piplus2_OWNPV_CHI2;
   Int_t           Piplus2_OWNPV_NDOF;
   Float_t         Piplus2_OWNPV_COV_[3][3];
   Double_t        Piplus2_IP_OWNPV;
   Double_t        Piplus2_IPCHI2_OWNPV;
   Double_t        Piplus2_ORIVX_X;
   Double_t        Piplus2_ORIVX_Y;
   Double_t        Piplus2_ORIVX_Z;
   Double_t        Piplus2_ORIVX_XERR;
   Double_t        Piplus2_ORIVX_YERR;
   Double_t        Piplus2_ORIVX_ZERR;
   Double_t        Piplus2_ORIVX_CHI2;
   Int_t           Piplus2_ORIVX_NDOF;
   Float_t         Piplus2_ORIVX_COV_[3][3];
   Double_t        Piplus2_P;
   Double_t        Piplus2_PT;
   Double_t        Piplus2_PE;
   Double_t        Piplus2_PX;
   Double_t        Piplus2_PY;
   Double_t        Piplus2_PZ;
   Double_t        Piplus2_M;
   Int_t           Piplus2_ID;
   Double_t        Piplus2_PIDe;
   Double_t        Piplus2_PIDmu;
   Double_t        Piplus2_PIDK;
   Double_t        Piplus2_PIDp;
   Double_t        Piplus2_ProbNNe;
   Double_t        Piplus2_ProbNNk;
   Double_t        Piplus2_ProbNNp;
   Double_t        Piplus2_ProbNNpi;
   Double_t        Piplus2_ProbNNmu;
   Double_t        Piplus2_ProbNNghost;
   Bool_t          Piplus2_hasMuon;
   Bool_t          Piplus2_isMuon;
   Bool_t          Piplus2_hasRich;
   Bool_t          Piplus2_hasCalo;
   Int_t           Piplus2_TRACK_Type;
   Int_t           Piplus2_TRACK_Key;
   Double_t        Piplus2_TRACK_CHI2NDOF;
   Double_t        Piplus2_TRACK_PCHI2;
   Double_t        Piplus2_TRACK_MatchCHI2;
   Double_t        Piplus2_TRACK_GhostProb;
   Double_t        Piplus2_TRACK_CloneDist;
   Double_t        Piplus2_TRACK_Likelihood;
   Double_t        Piplus2_X;
   Double_t        Piplus2_Y;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           L0Data_DiMuon_Pt;
   Int_t           L0Data_DiMuonProd_Pt1Pt2;
   Int_t           L0Data_Electron_Et;
   Int_t           L0Data_GlobalPi0_Et;
   Int_t           L0Data_Hadron_Et;
   Int_t           L0Data_LocalPi0_Et;
   Int_t           L0Data_Muon1_Pt;
   Int_t           L0Data_Muon1_Sgn;
   Int_t           L0Data_Muon2_Pt;
   Int_t           L0Data_Muon2_Sgn;
   Int_t           L0Data_Muon3_Pt;
   Int_t           L0Data_Muon3_Sgn;
   Int_t           L0Data_PUHits_Mult;
   Int_t           L0Data_PUPeak1_Cont;
   Int_t           L0Data_PUPeak1_Pos;
   Int_t           L0Data_PUPeak2_Cont;
   Int_t           L0Data_PUPeak2_Pos;
   Int_t           L0Data_Photon_Et;
   Int_t           L0Data_Spd_Mult;
   Int_t           L0Data_Sum_Et;
   Int_t           L0Data_Sum_Et_Next1;
   Int_t           L0Data_Sum_Et_Next2;
   Int_t           L0Data_Sum_Et_Prev1;
   Int_t           L0Data_Sum_Et_Prev2;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0HadronDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1TrackMVADecision;
   Int_t           Hlt1TwoTrackMVADecision;
   UInt_t          Hlt1nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]

   // List of branches
   TBranch        *b_Dplus_ENDVERTEX_X;   //!
   TBranch        *b_Dplus_ENDVERTEX_Y;   //!
   TBranch        *b_Dplus_ENDVERTEX_Z;   //!
   TBranch        *b_Dplus_ENDVERTEX_XERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_YERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_ZERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_CHI2;   //!
   TBranch        *b_Dplus_ENDVERTEX_NDOF;   //!
   TBranch        *b_Dplus_ENDVERTEX_COV_;   //!
   TBranch        *b_Dplus_OWNPV_X;   //!
   TBranch        *b_Dplus_OWNPV_Y;   //!
   TBranch        *b_Dplus_OWNPV_Z;   //!
   TBranch        *b_Dplus_OWNPV_XERR;   //!
   TBranch        *b_Dplus_OWNPV_YERR;   //!
   TBranch        *b_Dplus_OWNPV_ZERR;   //!
   TBranch        *b_Dplus_OWNPV_CHI2;   //!
   TBranch        *b_Dplus_OWNPV_NDOF;   //!
   TBranch        *b_Dplus_OWNPV_COV_;   //!
   TBranch        *b_Dplus_IP_OWNPV;   //!
   TBranch        *b_Dplus_IPCHI2_OWNPV;   //!
   TBranch        *b_Dplus_FD_OWNPV;   //!
   TBranch        *b_Dplus_FDCHI2_OWNPV;   //!
   TBranch        *b_Dplus_DIRA_OWNPV;   //!
   TBranch        *b_Dplus_P;   //!
   TBranch        *b_Dplus_PT;   //!
   TBranch        *b_Dplus_PE;   //!
   TBranch        *b_Dplus_PX;   //!
   TBranch        *b_Dplus_PY;   //!
   TBranch        *b_Dplus_PZ;   //!
   TBranch        *b_Dplus_MM;   //!
   TBranch        *b_Dplus_MMERR;   //!
   TBranch        *b_Dplus_M;   //!
   TBranch        *b_Dplus_ID;   //!
   TBranch        *b_Dplus_TAU;   //!
   TBranch        *b_Dplus_TAUERR;   //!
   TBranch        *b_Dplus_TAUCHI2;   //!
   TBranch        *b_Dplus_X;   //!
   TBranch        *b_Dplus_Y;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Kminus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Kminus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Kminus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Kminus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Kminus_OWNPV_X;   //!
   TBranch        *b_Kminus_OWNPV_Y;   //!
   TBranch        *b_Kminus_OWNPV_Z;   //!
   TBranch        *b_Kminus_OWNPV_XERR;   //!
   TBranch        *b_Kminus_OWNPV_YERR;   //!
   TBranch        *b_Kminus_OWNPV_ZERR;   //!
   TBranch        *b_Kminus_OWNPV_CHI2;   //!
   TBranch        *b_Kminus_OWNPV_NDOF;   //!
   TBranch        *b_Kminus_OWNPV_COV_;   //!
   TBranch        *b_Kminus_IP_OWNPV;   //!
   TBranch        *b_Kminus_IPCHI2_OWNPV;   //!
   TBranch        *b_Kminus_ORIVX_X;   //!
   TBranch        *b_Kminus_ORIVX_Y;   //!
   TBranch        *b_Kminus_ORIVX_Z;   //!
   TBranch        *b_Kminus_ORIVX_XERR;   //!
   TBranch        *b_Kminus_ORIVX_YERR;   //!
   TBranch        *b_Kminus_ORIVX_ZERR;   //!
   TBranch        *b_Kminus_ORIVX_CHI2;   //!
   TBranch        *b_Kminus_ORIVX_NDOF;   //!
   TBranch        *b_Kminus_ORIVX_COV_;   //!
   TBranch        *b_Kminus_P;   //!
   TBranch        *b_Kminus_PT;   //!
   TBranch        *b_Kminus_PE;   //!
   TBranch        *b_Kminus_PX;   //!
   TBranch        *b_Kminus_PY;   //!
   TBranch        *b_Kminus_PZ;   //!
   TBranch        *b_Kminus_M;   //!
   TBranch        *b_Kminus_ID;   //!
   TBranch        *b_Kminus_PIDe;   //!
   TBranch        *b_Kminus_PIDmu;   //!
   TBranch        *b_Kminus_PIDK;   //!
   TBranch        *b_Kminus_PIDp;   //!
   TBranch        *b_Kminus_ProbNNe;   //!
   TBranch        *b_Kminus_ProbNNk;   //!
   TBranch        *b_Kminus_ProbNNp;   //!
   TBranch        *b_Kminus_ProbNNpi;   //!
   TBranch        *b_Kminus_ProbNNmu;   //!
   TBranch        *b_Kminus_ProbNNghost;   //!
   TBranch        *b_Kminus_hasMuon;   //!
   TBranch        *b_Kminus_isMuon;   //!
   TBranch        *b_Kminus_hasRich;   //!
   TBranch        *b_Kminus_hasCalo;   //!
   TBranch        *b_Kminus_TRACK_Type;   //!
   TBranch        *b_Kminus_TRACK_Key;   //!
   TBranch        *b_Kminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_Kminus_TRACK_PCHI2;   //!
   TBranch        *b_Kminus_TRACK_MatchCHI2;   //!
   TBranch        *b_Kminus_TRACK_GhostProb;   //!
   TBranch        *b_Kminus_TRACK_CloneDist;   //!
   TBranch        *b_Kminus_TRACK_Likelihood;   //!
   TBranch        *b_Kminus_X;   //!
   TBranch        *b_Kminus_Y;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Piplus1_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Piplus1_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Piplus1_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Piplus1_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Piplus1_OWNPV_X;   //!
   TBranch        *b_Piplus1_OWNPV_Y;   //!
   TBranch        *b_Piplus1_OWNPV_Z;   //!
   TBranch        *b_Piplus1_OWNPV_XERR;   //!
   TBranch        *b_Piplus1_OWNPV_YERR;   //!
   TBranch        *b_Piplus1_OWNPV_ZERR;   //!
   TBranch        *b_Piplus1_OWNPV_CHI2;   //!
   TBranch        *b_Piplus1_OWNPV_NDOF;   //!
   TBranch        *b_Piplus1_OWNPV_COV_;   //!
   TBranch        *b_Piplus1_IP_OWNPV;   //!
   TBranch        *b_Piplus1_IPCHI2_OWNPV;   //!
   TBranch        *b_Piplus1_ORIVX_X;   //!
   TBranch        *b_Piplus1_ORIVX_Y;   //!
   TBranch        *b_Piplus1_ORIVX_Z;   //!
   TBranch        *b_Piplus1_ORIVX_XERR;   //!
   TBranch        *b_Piplus1_ORIVX_YERR;   //!
   TBranch        *b_Piplus1_ORIVX_ZERR;   //!
   TBranch        *b_Piplus1_ORIVX_CHI2;   //!
   TBranch        *b_Piplus1_ORIVX_NDOF;   //!
   TBranch        *b_Piplus1_ORIVX_COV_;   //!
   TBranch        *b_Piplus1_P;   //!
   TBranch        *b_Piplus1_PT;   //!
   TBranch        *b_Piplus1_PE;   //!
   TBranch        *b_Piplus1_PX;   //!
   TBranch        *b_Piplus1_PY;   //!
   TBranch        *b_Piplus1_PZ;   //!
   TBranch        *b_Piplus1_M;   //!
   TBranch        *b_Piplus1_ID;   //!
   TBranch        *b_Piplus1_PIDe;   //!
   TBranch        *b_Piplus1_PIDmu;   //!
   TBranch        *b_Piplus1_PIDK;   //!
   TBranch        *b_Piplus1_PIDp;   //!
   TBranch        *b_Piplus1_ProbNNe;   //!
   TBranch        *b_Piplus1_ProbNNk;   //!
   TBranch        *b_Piplus1_ProbNNp;   //!
   TBranch        *b_Piplus1_ProbNNpi;   //!
   TBranch        *b_Piplus1_ProbNNmu;   //!
   TBranch        *b_Piplus1_ProbNNghost;   //!
   TBranch        *b_Piplus1_hasMuon;   //!
   TBranch        *b_Piplus1_isMuon;   //!
   TBranch        *b_Piplus1_hasRich;   //!
   TBranch        *b_Piplus1_hasCalo;   //!
   TBranch        *b_Piplus1_TRACK_Type;   //!
   TBranch        *b_Piplus1_TRACK_Key;   //!
   TBranch        *b_Piplus1_TRACK_CHI2NDOF;   //!
   TBranch        *b_Piplus1_TRACK_PCHI2;   //!
   TBranch        *b_Piplus1_TRACK_MatchCHI2;   //!
   TBranch        *b_Piplus1_TRACK_GhostProb;   //!
   TBranch        *b_Piplus1_TRACK_CloneDist;   //!
   TBranch        *b_Piplus1_TRACK_Likelihood;   //!
   TBranch        *b_Piplus1_X;   //!
   TBranch        *b_Piplus1_Y;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Piplus2_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Piplus2_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Piplus2_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Piplus2_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Piplus2_OWNPV_X;   //!
   TBranch        *b_Piplus2_OWNPV_Y;   //!
   TBranch        *b_Piplus2_OWNPV_Z;   //!
   TBranch        *b_Piplus2_OWNPV_XERR;   //!
   TBranch        *b_Piplus2_OWNPV_YERR;   //!
   TBranch        *b_Piplus2_OWNPV_ZERR;   //!
   TBranch        *b_Piplus2_OWNPV_CHI2;   //!
   TBranch        *b_Piplus2_OWNPV_NDOF;   //!
   TBranch        *b_Piplus2_OWNPV_COV_;   //!
   TBranch        *b_Piplus2_IP_OWNPV;   //!
   TBranch        *b_Piplus2_IPCHI2_OWNPV;   //!
   TBranch        *b_Piplus2_ORIVX_X;   //!
   TBranch        *b_Piplus2_ORIVX_Y;   //!
   TBranch        *b_Piplus2_ORIVX_Z;   //!
   TBranch        *b_Piplus2_ORIVX_XERR;   //!
   TBranch        *b_Piplus2_ORIVX_YERR;   //!
   TBranch        *b_Piplus2_ORIVX_ZERR;   //!
   TBranch        *b_Piplus2_ORIVX_CHI2;   //!
   TBranch        *b_Piplus2_ORIVX_NDOF;   //!
   TBranch        *b_Piplus2_ORIVX_COV_;   //!
   TBranch        *b_Piplus2_P;   //!
   TBranch        *b_Piplus2_PT;   //!
   TBranch        *b_Piplus2_PE;   //!
   TBranch        *b_Piplus2_PX;   //!
   TBranch        *b_Piplus2_PY;   //!
   TBranch        *b_Piplus2_PZ;   //!
   TBranch        *b_Piplus2_M;   //!
   TBranch        *b_Piplus2_ID;   //!
   TBranch        *b_Piplus2_PIDe;   //!
   TBranch        *b_Piplus2_PIDmu;   //!
   TBranch        *b_Piplus2_PIDK;   //!
   TBranch        *b_Piplus2_PIDp;   //!
   TBranch        *b_Piplus2_ProbNNe;   //!
   TBranch        *b_Piplus2_ProbNNk;   //!
   TBranch        *b_Piplus2_ProbNNp;   //!
   TBranch        *b_Piplus2_ProbNNpi;   //!
   TBranch        *b_Piplus2_ProbNNmu;   //!
   TBranch        *b_Piplus2_ProbNNghost;   //!
   TBranch        *b_Piplus2_hasMuon;   //!
   TBranch        *b_Piplus2_isMuon;   //!
   TBranch        *b_Piplus2_hasRich;   //!
   TBranch        *b_Piplus2_hasCalo;   //!
   TBranch        *b_Piplus2_TRACK_Type;   //!
   TBranch        *b_Piplus2_TRACK_Key;   //!
   TBranch        *b_Piplus2_TRACK_CHI2NDOF;   //!
   TBranch        *b_Piplus2_TRACK_PCHI2;   //!
   TBranch        *b_Piplus2_TRACK_MatchCHI2;   //!
   TBranch        *b_Piplus2_TRACK_GhostProb;   //!
   TBranch        *b_Piplus2_TRACK_CloneDist;   //!
   TBranch        *b_Piplus2_TRACK_Likelihood;   //!
   TBranch        *b_Piplus2_X;   //!
   TBranch        *b_Piplus2_Y;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_L0Data_DiMuon_Pt;   //!
   TBranch        *b_L0Data_DiMuonProd_Pt1Pt2;   //!
   TBranch        *b_L0Data_Electron_Et;   //!
   TBranch        *b_L0Data_GlobalPi0_Et;   //!
   TBranch        *b_L0Data_Hadron_Et;   //!
   TBranch        *b_L0Data_LocalPi0_Et;   //!
   TBranch        *b_L0Data_Muon1_Pt;   //!
   TBranch        *b_L0Data_Muon1_Sgn;   //!
   TBranch        *b_L0Data_Muon2_Pt;   //!
   TBranch        *b_L0Data_Muon2_Sgn;   //!
   TBranch        *b_L0Data_Muon3_Pt;   //!
   TBranch        *b_L0Data_Muon3_Sgn;   //!
   TBranch        *b_L0Data_PUHits_Mult;   //!
   TBranch        *b_L0Data_PUPeak1_Cont;   //!
   TBranch        *b_L0Data_PUPeak1_Pos;   //!
   TBranch        *b_L0Data_PUPeak2_Cont;   //!
   TBranch        *b_L0Data_PUPeak2_Pos;   //!
   TBranch        *b_L0Data_Photon_Et;   //!
   TBranch        *b_L0Data_Spd_Mult;   //!
   TBranch        *b_L0Data_Sum_Et;   //!
   TBranch        *b_L0Data_Sum_Et_Next1;   //!
   TBranch        *b_L0Data_Sum_Et_Next2;   //!
   TBranch        *b_L0Data_Sum_Et_Prev1;   //!
   TBranch        *b_L0Data_Sum_Et_Prev2;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0HadronDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1TrackMVADecision;   //!
   TBranch        *b_Hlt1TwoTrackMVADecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!

   DpToKmPipPip_class(TTree *tree=0);
   virtual ~DpToKmPipPip_class();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

  virtual void fitDplusMass(TH1 *hist, TString name, double *results);
  virtual void plotMasses(double *massArray1, double *errMassArray1,
                          double *xArray, double *xErrArray, int nBins,
                          TString xLabel, TString yLabel, TString fileName,
                          TString title);

  virtual void plotBothMasses(double *massArray1, double *errMassArray1,
                              double *massArray2, double *errMassArray2,
                              double *xArray, double *xErrArray, int nBins,
                              TString xLabel, TString yLabel, TString fileName,
                              TString title);
  virtual void plotBothAverages(double *massDp, double *errMassDp,
                                double *massDm, double *errMassDm,
                                double *xArray, double *xErrArray, int nBins,
                                TString xLabel, TString yLabel,
                                TString fileName, TString title, double yMin,
                                double yMax);
  virtual void printSomeResults(ofstream &outfile, int precision, int valWidth,
                                int errWidth, double *valArray,
                                double *errArray);
  virtual void simplePlot(TString fileName, TH1 *hist);


};

#endif

#ifdef DpToKmPipPip_class_cxx
DpToKmPipPip_class::DpToKmPipPip_class(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("TeslaTuples_Sept07A_0x11381609_00_10.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("TeslaTuples_Sept07A_0x11381609_00_10.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("TeslaTuples_Sept07A_0x11381609_00_10.root:/DpToKmPipPip");
      dir->GetObject("DecayTree",tree);

   }
   Init(tree);
}

DpToKmPipPip_class::~DpToKmPipPip_class()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DpToKmPipPip_class::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DpToKmPipPip_class::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DpToKmPipPip_class::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Dplus_ENDVERTEX_X", &Dplus_ENDVERTEX_X, &b_Dplus_ENDVERTEX_X);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_Y", &Dplus_ENDVERTEX_Y, &b_Dplus_ENDVERTEX_Y);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_Z", &Dplus_ENDVERTEX_Z, &b_Dplus_ENDVERTEX_Z);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_XERR", &Dplus_ENDVERTEX_XERR, &b_Dplus_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_YERR", &Dplus_ENDVERTEX_YERR, &b_Dplus_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_ZERR", &Dplus_ENDVERTEX_ZERR, &b_Dplus_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_CHI2", &Dplus_ENDVERTEX_CHI2, &b_Dplus_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_NDOF", &Dplus_ENDVERTEX_NDOF, &b_Dplus_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_COV_", Dplus_ENDVERTEX_COV_, &b_Dplus_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Dplus_OWNPV_X", &Dplus_OWNPV_X, &b_Dplus_OWNPV_X);
   fChain->SetBranchAddress("Dplus_OWNPV_Y", &Dplus_OWNPV_Y, &b_Dplus_OWNPV_Y);
   fChain->SetBranchAddress("Dplus_OWNPV_Z", &Dplus_OWNPV_Z, &b_Dplus_OWNPV_Z);
   fChain->SetBranchAddress("Dplus_OWNPV_XERR", &Dplus_OWNPV_XERR, &b_Dplus_OWNPV_XERR);
   fChain->SetBranchAddress("Dplus_OWNPV_YERR", &Dplus_OWNPV_YERR, &b_Dplus_OWNPV_YERR);
   fChain->SetBranchAddress("Dplus_OWNPV_ZERR", &Dplus_OWNPV_ZERR, &b_Dplus_OWNPV_ZERR);
   fChain->SetBranchAddress("Dplus_OWNPV_CHI2", &Dplus_OWNPV_CHI2, &b_Dplus_OWNPV_CHI2);
   fChain->SetBranchAddress("Dplus_OWNPV_NDOF", &Dplus_OWNPV_NDOF, &b_Dplus_OWNPV_NDOF);
   fChain->SetBranchAddress("Dplus_OWNPV_COV_", Dplus_OWNPV_COV_, &b_Dplus_OWNPV_COV_);
   fChain->SetBranchAddress("Dplus_IP_OWNPV", &Dplus_IP_OWNPV, &b_Dplus_IP_OWNPV);
   fChain->SetBranchAddress("Dplus_IPCHI2_OWNPV", &Dplus_IPCHI2_OWNPV, &b_Dplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Dplus_FD_OWNPV", &Dplus_FD_OWNPV, &b_Dplus_FD_OWNPV);
   fChain->SetBranchAddress("Dplus_FDCHI2_OWNPV", &Dplus_FDCHI2_OWNPV, &b_Dplus_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Dplus_DIRA_OWNPV", &Dplus_DIRA_OWNPV, &b_Dplus_DIRA_OWNPV);
   fChain->SetBranchAddress("Dplus_P", &Dplus_P, &b_Dplus_P);
   fChain->SetBranchAddress("Dplus_PT", &Dplus_PT, &b_Dplus_PT);
   fChain->SetBranchAddress("Dplus_PE", &Dplus_PE, &b_Dplus_PE);
   fChain->SetBranchAddress("Dplus_PX", &Dplus_PX, &b_Dplus_PX);
   fChain->SetBranchAddress("Dplus_PY", &Dplus_PY, &b_Dplus_PY);
   fChain->SetBranchAddress("Dplus_PZ", &Dplus_PZ, &b_Dplus_PZ);
   fChain->SetBranchAddress("Dplus_MM", &Dplus_MM, &b_Dplus_MM);
   fChain->SetBranchAddress("Dplus_MMERR", &Dplus_MMERR, &b_Dplus_MMERR);
   fChain->SetBranchAddress("Dplus_M", &Dplus_M, &b_Dplus_M);
   fChain->SetBranchAddress("Dplus_ID", &Dplus_ID, &b_Dplus_ID);
   fChain->SetBranchAddress("Dplus_TAU", &Dplus_TAU, &b_Dplus_TAU);
   fChain->SetBranchAddress("Dplus_TAUERR", &Dplus_TAUERR, &b_Dplus_TAUERR);
   fChain->SetBranchAddress("Dplus_TAUCHI2", &Dplus_TAUCHI2, &b_Dplus_TAUCHI2);
   fChain->SetBranchAddress("Dplus_X", &Dplus_X, &b_Dplus_X);
   fChain->SetBranchAddress("Dplus_Y", &Dplus_Y, &b_Dplus_Y);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNe", &Kminus_MC12TuneV2_ProbNNe, &b_Kminus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNmu", &Kminus_MC12TuneV2_ProbNNmu, &b_Kminus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNpi", &Kminus_MC12TuneV2_ProbNNpi, &b_Kminus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNk", &Kminus_MC12TuneV2_ProbNNk, &b_Kminus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNp", &Kminus_MC12TuneV2_ProbNNp, &b_Kminus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Kminus_MC12TuneV2_ProbNNghost", &Kminus_MC12TuneV2_ProbNNghost, &b_Kminus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNe", &Kminus_MC12TuneV3_ProbNNe, &b_Kminus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNmu", &Kminus_MC12TuneV3_ProbNNmu, &b_Kminus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNpi", &Kminus_MC12TuneV3_ProbNNpi, &b_Kminus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNk", &Kminus_MC12TuneV3_ProbNNk, &b_Kminus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNp", &Kminus_MC12TuneV3_ProbNNp, &b_Kminus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Kminus_MC12TuneV3_ProbNNghost", &Kminus_MC12TuneV3_ProbNNghost, &b_Kminus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNe", &Kminus_MC12TuneV4_ProbNNe, &b_Kminus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNmu", &Kminus_MC12TuneV4_ProbNNmu, &b_Kminus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNpi", &Kminus_MC12TuneV4_ProbNNpi, &b_Kminus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNk", &Kminus_MC12TuneV4_ProbNNk, &b_Kminus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNp", &Kminus_MC12TuneV4_ProbNNp, &b_Kminus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Kminus_MC12TuneV4_ProbNNghost", &Kminus_MC12TuneV4_ProbNNghost, &b_Kminus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNe", &Kminus_MC15TuneV1_ProbNNe, &b_Kminus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNmu", &Kminus_MC15TuneV1_ProbNNmu, &b_Kminus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNpi", &Kminus_MC15TuneV1_ProbNNpi, &b_Kminus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNk", &Kminus_MC15TuneV1_ProbNNk, &b_Kminus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNp", &Kminus_MC15TuneV1_ProbNNp, &b_Kminus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Kminus_MC15TuneV1_ProbNNghost", &Kminus_MC15TuneV1_ProbNNghost, &b_Kminus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Kminus_OWNPV_X", &Kminus_OWNPV_X, &b_Kminus_OWNPV_X);
   fChain->SetBranchAddress("Kminus_OWNPV_Y", &Kminus_OWNPV_Y, &b_Kminus_OWNPV_Y);
   fChain->SetBranchAddress("Kminus_OWNPV_Z", &Kminus_OWNPV_Z, &b_Kminus_OWNPV_Z);
   fChain->SetBranchAddress("Kminus_OWNPV_XERR", &Kminus_OWNPV_XERR, &b_Kminus_OWNPV_XERR);
   fChain->SetBranchAddress("Kminus_OWNPV_YERR", &Kminus_OWNPV_YERR, &b_Kminus_OWNPV_YERR);
   fChain->SetBranchAddress("Kminus_OWNPV_ZERR", &Kminus_OWNPV_ZERR, &b_Kminus_OWNPV_ZERR);
   fChain->SetBranchAddress("Kminus_OWNPV_CHI2", &Kminus_OWNPV_CHI2, &b_Kminus_OWNPV_CHI2);
   fChain->SetBranchAddress("Kminus_OWNPV_NDOF", &Kminus_OWNPV_NDOF, &b_Kminus_OWNPV_NDOF);
   fChain->SetBranchAddress("Kminus_OWNPV_COV_", Kminus_OWNPV_COV_, &b_Kminus_OWNPV_COV_);
   fChain->SetBranchAddress("Kminus_IP_OWNPV", &Kminus_IP_OWNPV, &b_Kminus_IP_OWNPV);
   fChain->SetBranchAddress("Kminus_IPCHI2_OWNPV", &Kminus_IPCHI2_OWNPV, &b_Kminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Kminus_ORIVX_X", &Kminus_ORIVX_X, &b_Kminus_ORIVX_X);
   fChain->SetBranchAddress("Kminus_ORIVX_Y", &Kminus_ORIVX_Y, &b_Kminus_ORIVX_Y);
   fChain->SetBranchAddress("Kminus_ORIVX_Z", &Kminus_ORIVX_Z, &b_Kminus_ORIVX_Z);
   fChain->SetBranchAddress("Kminus_ORIVX_XERR", &Kminus_ORIVX_XERR, &b_Kminus_ORIVX_XERR);
   fChain->SetBranchAddress("Kminus_ORIVX_YERR", &Kminus_ORIVX_YERR, &b_Kminus_ORIVX_YERR);
   fChain->SetBranchAddress("Kminus_ORIVX_ZERR", &Kminus_ORIVX_ZERR, &b_Kminus_ORIVX_ZERR);
   fChain->SetBranchAddress("Kminus_ORIVX_CHI2", &Kminus_ORIVX_CHI2, &b_Kminus_ORIVX_CHI2);
   fChain->SetBranchAddress("Kminus_ORIVX_NDOF", &Kminus_ORIVX_NDOF, &b_Kminus_ORIVX_NDOF);
   fChain->SetBranchAddress("Kminus_ORIVX_COV_", Kminus_ORIVX_COV_, &b_Kminus_ORIVX_COV_);
   fChain->SetBranchAddress("Kminus_P", &Kminus_P, &b_Kminus_P);
   fChain->SetBranchAddress("Kminus_PT", &Kminus_PT, &b_Kminus_PT);
   fChain->SetBranchAddress("Kminus_PE", &Kminus_PE, &b_Kminus_PE);
   fChain->SetBranchAddress("Kminus_PX", &Kminus_PX, &b_Kminus_PX);
   fChain->SetBranchAddress("Kminus_PY", &Kminus_PY, &b_Kminus_PY);
   fChain->SetBranchAddress("Kminus_PZ", &Kminus_PZ, &b_Kminus_PZ);
   fChain->SetBranchAddress("Kminus_M", &Kminus_M, &b_Kminus_M);
   fChain->SetBranchAddress("Kminus_ID", &Kminus_ID, &b_Kminus_ID);
   fChain->SetBranchAddress("Kminus_PIDe", &Kminus_PIDe, &b_Kminus_PIDe);
   fChain->SetBranchAddress("Kminus_PIDmu", &Kminus_PIDmu, &b_Kminus_PIDmu);
   fChain->SetBranchAddress("Kminus_PIDK", &Kminus_PIDK, &b_Kminus_PIDK);
   fChain->SetBranchAddress("Kminus_PIDp", &Kminus_PIDp, &b_Kminus_PIDp);
   fChain->SetBranchAddress("Kminus_ProbNNe", &Kminus_ProbNNe, &b_Kminus_ProbNNe);
   fChain->SetBranchAddress("Kminus_ProbNNk", &Kminus_ProbNNk, &b_Kminus_ProbNNk);
   fChain->SetBranchAddress("Kminus_ProbNNp", &Kminus_ProbNNp, &b_Kminus_ProbNNp);
   fChain->SetBranchAddress("Kminus_ProbNNpi", &Kminus_ProbNNpi, &b_Kminus_ProbNNpi);
   fChain->SetBranchAddress("Kminus_ProbNNmu", &Kminus_ProbNNmu, &b_Kminus_ProbNNmu);
   fChain->SetBranchAddress("Kminus_ProbNNghost", &Kminus_ProbNNghost, &b_Kminus_ProbNNghost);
   fChain->SetBranchAddress("Kminus_hasMuon", &Kminus_hasMuon, &b_Kminus_hasMuon);
   fChain->SetBranchAddress("Kminus_isMuon", &Kminus_isMuon, &b_Kminus_isMuon);
   fChain->SetBranchAddress("Kminus_hasRich", &Kminus_hasRich, &b_Kminus_hasRich);
   fChain->SetBranchAddress("Kminus_hasCalo", &Kminus_hasCalo, &b_Kminus_hasCalo);
   fChain->SetBranchAddress("Kminus_TRACK_Type", &Kminus_TRACK_Type, &b_Kminus_TRACK_Type);
   fChain->SetBranchAddress("Kminus_TRACK_Key", &Kminus_TRACK_Key, &b_Kminus_TRACK_Key);
   fChain->SetBranchAddress("Kminus_TRACK_CHI2NDOF", &Kminus_TRACK_CHI2NDOF, &b_Kminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Kminus_TRACK_PCHI2", &Kminus_TRACK_PCHI2, &b_Kminus_TRACK_PCHI2);
   fChain->SetBranchAddress("Kminus_TRACK_MatchCHI2", &Kminus_TRACK_MatchCHI2, &b_Kminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Kminus_TRACK_GhostProb", &Kminus_TRACK_GhostProb, &b_Kminus_TRACK_GhostProb);
   fChain->SetBranchAddress("Kminus_TRACK_CloneDist", &Kminus_TRACK_CloneDist, &b_Kminus_TRACK_CloneDist);
   fChain->SetBranchAddress("Kminus_TRACK_Likelihood", &Kminus_TRACK_Likelihood, &b_Kminus_TRACK_Likelihood);
   fChain->SetBranchAddress("Kminus_X", &Kminus_X, &b_Kminus_X);
   fChain->SetBranchAddress("Kminus_Y", &Kminus_Y, &b_Kminus_Y);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNe", &Piplus1_MC12TuneV2_ProbNNe, &b_Piplus1_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNmu", &Piplus1_MC12TuneV2_ProbNNmu, &b_Piplus1_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNpi", &Piplus1_MC12TuneV2_ProbNNpi, &b_Piplus1_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNk", &Piplus1_MC12TuneV2_ProbNNk, &b_Piplus1_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNp", &Piplus1_MC12TuneV2_ProbNNp, &b_Piplus1_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Piplus1_MC12TuneV2_ProbNNghost", &Piplus1_MC12TuneV2_ProbNNghost, &b_Piplus1_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNe", &Piplus1_MC12TuneV3_ProbNNe, &b_Piplus1_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNmu", &Piplus1_MC12TuneV3_ProbNNmu, &b_Piplus1_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNpi", &Piplus1_MC12TuneV3_ProbNNpi, &b_Piplus1_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNk", &Piplus1_MC12TuneV3_ProbNNk, &b_Piplus1_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNp", &Piplus1_MC12TuneV3_ProbNNp, &b_Piplus1_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Piplus1_MC12TuneV3_ProbNNghost", &Piplus1_MC12TuneV3_ProbNNghost, &b_Piplus1_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNe", &Piplus1_MC12TuneV4_ProbNNe, &b_Piplus1_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNmu", &Piplus1_MC12TuneV4_ProbNNmu, &b_Piplus1_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNpi", &Piplus1_MC12TuneV4_ProbNNpi, &b_Piplus1_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNk", &Piplus1_MC12TuneV4_ProbNNk, &b_Piplus1_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNp", &Piplus1_MC12TuneV4_ProbNNp, &b_Piplus1_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Piplus1_MC12TuneV4_ProbNNghost", &Piplus1_MC12TuneV4_ProbNNghost, &b_Piplus1_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNe", &Piplus1_MC15TuneV1_ProbNNe, &b_Piplus1_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNmu", &Piplus1_MC15TuneV1_ProbNNmu, &b_Piplus1_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNpi", &Piplus1_MC15TuneV1_ProbNNpi, &b_Piplus1_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNk", &Piplus1_MC15TuneV1_ProbNNk, &b_Piplus1_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNp", &Piplus1_MC15TuneV1_ProbNNp, &b_Piplus1_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Piplus1_MC15TuneV1_ProbNNghost", &Piplus1_MC15TuneV1_ProbNNghost, &b_Piplus1_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Piplus1_OWNPV_X", &Piplus1_OWNPV_X, &b_Piplus1_OWNPV_X);
   fChain->SetBranchAddress("Piplus1_OWNPV_Y", &Piplus1_OWNPV_Y, &b_Piplus1_OWNPV_Y);
   fChain->SetBranchAddress("Piplus1_OWNPV_Z", &Piplus1_OWNPV_Z, &b_Piplus1_OWNPV_Z);
   fChain->SetBranchAddress("Piplus1_OWNPV_XERR", &Piplus1_OWNPV_XERR, &b_Piplus1_OWNPV_XERR);
   fChain->SetBranchAddress("Piplus1_OWNPV_YERR", &Piplus1_OWNPV_YERR, &b_Piplus1_OWNPV_YERR);
   fChain->SetBranchAddress("Piplus1_OWNPV_ZERR", &Piplus1_OWNPV_ZERR, &b_Piplus1_OWNPV_ZERR);
   fChain->SetBranchAddress("Piplus1_OWNPV_CHI2", &Piplus1_OWNPV_CHI2, &b_Piplus1_OWNPV_CHI2);
   fChain->SetBranchAddress("Piplus1_OWNPV_NDOF", &Piplus1_OWNPV_NDOF, &b_Piplus1_OWNPV_NDOF);
   fChain->SetBranchAddress("Piplus1_OWNPV_COV_", Piplus1_OWNPV_COV_, &b_Piplus1_OWNPV_COV_);
   fChain->SetBranchAddress("Piplus1_IP_OWNPV", &Piplus1_IP_OWNPV, &b_Piplus1_IP_OWNPV);
   fChain->SetBranchAddress("Piplus1_IPCHI2_OWNPV", &Piplus1_IPCHI2_OWNPV, &b_Piplus1_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Piplus1_ORIVX_X", &Piplus1_ORIVX_X, &b_Piplus1_ORIVX_X);
   fChain->SetBranchAddress("Piplus1_ORIVX_Y", &Piplus1_ORIVX_Y, &b_Piplus1_ORIVX_Y);
   fChain->SetBranchAddress("Piplus1_ORIVX_Z", &Piplus1_ORIVX_Z, &b_Piplus1_ORIVX_Z);
   fChain->SetBranchAddress("Piplus1_ORIVX_XERR", &Piplus1_ORIVX_XERR, &b_Piplus1_ORIVX_XERR);
   fChain->SetBranchAddress("Piplus1_ORIVX_YERR", &Piplus1_ORIVX_YERR, &b_Piplus1_ORIVX_YERR);
   fChain->SetBranchAddress("Piplus1_ORIVX_ZERR", &Piplus1_ORIVX_ZERR, &b_Piplus1_ORIVX_ZERR);
   fChain->SetBranchAddress("Piplus1_ORIVX_CHI2", &Piplus1_ORIVX_CHI2, &b_Piplus1_ORIVX_CHI2);
   fChain->SetBranchAddress("Piplus1_ORIVX_NDOF", &Piplus1_ORIVX_NDOF, &b_Piplus1_ORIVX_NDOF);
   fChain->SetBranchAddress("Piplus1_ORIVX_COV_", Piplus1_ORIVX_COV_, &b_Piplus1_ORIVX_COV_);
   fChain->SetBranchAddress("Piplus1_P", &Piplus1_P, &b_Piplus1_P);
   fChain->SetBranchAddress("Piplus1_PT", &Piplus1_PT, &b_Piplus1_PT);
   fChain->SetBranchAddress("Piplus1_PE", &Piplus1_PE, &b_Piplus1_PE);
   fChain->SetBranchAddress("Piplus1_PX", &Piplus1_PX, &b_Piplus1_PX);
   fChain->SetBranchAddress("Piplus1_PY", &Piplus1_PY, &b_Piplus1_PY);
   fChain->SetBranchAddress("Piplus1_PZ", &Piplus1_PZ, &b_Piplus1_PZ);
   fChain->SetBranchAddress("Piplus1_M", &Piplus1_M, &b_Piplus1_M);
   fChain->SetBranchAddress("Piplus1_ID", &Piplus1_ID, &b_Piplus1_ID);
   fChain->SetBranchAddress("Piplus1_PIDe", &Piplus1_PIDe, &b_Piplus1_PIDe);
   fChain->SetBranchAddress("Piplus1_PIDmu", &Piplus1_PIDmu, &b_Piplus1_PIDmu);
   fChain->SetBranchAddress("Piplus1_PIDK", &Piplus1_PIDK, &b_Piplus1_PIDK);
   fChain->SetBranchAddress("Piplus1_PIDp", &Piplus1_PIDp, &b_Piplus1_PIDp);
   fChain->SetBranchAddress("Piplus1_ProbNNe", &Piplus1_ProbNNe, &b_Piplus1_ProbNNe);
   fChain->SetBranchAddress("Piplus1_ProbNNk", &Piplus1_ProbNNk, &b_Piplus1_ProbNNk);
   fChain->SetBranchAddress("Piplus1_ProbNNp", &Piplus1_ProbNNp, &b_Piplus1_ProbNNp);
   fChain->SetBranchAddress("Piplus1_ProbNNpi", &Piplus1_ProbNNpi, &b_Piplus1_ProbNNpi);
   fChain->SetBranchAddress("Piplus1_ProbNNmu", &Piplus1_ProbNNmu, &b_Piplus1_ProbNNmu);
   fChain->SetBranchAddress("Piplus1_ProbNNghost", &Piplus1_ProbNNghost, &b_Piplus1_ProbNNghost);
   fChain->SetBranchAddress("Piplus1_hasMuon", &Piplus1_hasMuon, &b_Piplus1_hasMuon);
   fChain->SetBranchAddress("Piplus1_isMuon", &Piplus1_isMuon, &b_Piplus1_isMuon);
   fChain->SetBranchAddress("Piplus1_hasRich", &Piplus1_hasRich, &b_Piplus1_hasRich);
   fChain->SetBranchAddress("Piplus1_hasCalo", &Piplus1_hasCalo, &b_Piplus1_hasCalo);
   fChain->SetBranchAddress("Piplus1_TRACK_Type", &Piplus1_TRACK_Type, &b_Piplus1_TRACK_Type);
   fChain->SetBranchAddress("Piplus1_TRACK_Key", &Piplus1_TRACK_Key, &b_Piplus1_TRACK_Key);
   fChain->SetBranchAddress("Piplus1_TRACK_CHI2NDOF", &Piplus1_TRACK_CHI2NDOF, &b_Piplus1_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Piplus1_TRACK_PCHI2", &Piplus1_TRACK_PCHI2, &b_Piplus1_TRACK_PCHI2);
   fChain->SetBranchAddress("Piplus1_TRACK_MatchCHI2", &Piplus1_TRACK_MatchCHI2, &b_Piplus1_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Piplus1_TRACK_GhostProb", &Piplus1_TRACK_GhostProb, &b_Piplus1_TRACK_GhostProb);
   fChain->SetBranchAddress("Piplus1_TRACK_CloneDist", &Piplus1_TRACK_CloneDist, &b_Piplus1_TRACK_CloneDist);
   fChain->SetBranchAddress("Piplus1_TRACK_Likelihood", &Piplus1_TRACK_Likelihood, &b_Piplus1_TRACK_Likelihood);
   fChain->SetBranchAddress("Piplus1_X", &Piplus1_X, &b_Piplus1_X);
   fChain->SetBranchAddress("Piplus1_Y", &Piplus1_Y, &b_Piplus1_Y);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNe", &Piplus2_MC12TuneV2_ProbNNe, &b_Piplus2_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNmu", &Piplus2_MC12TuneV2_ProbNNmu, &b_Piplus2_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNpi", &Piplus2_MC12TuneV2_ProbNNpi, &b_Piplus2_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNk", &Piplus2_MC12TuneV2_ProbNNk, &b_Piplus2_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNp", &Piplus2_MC12TuneV2_ProbNNp, &b_Piplus2_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Piplus2_MC12TuneV2_ProbNNghost", &Piplus2_MC12TuneV2_ProbNNghost, &b_Piplus2_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNe", &Piplus2_MC12TuneV3_ProbNNe, &b_Piplus2_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNmu", &Piplus2_MC12TuneV3_ProbNNmu, &b_Piplus2_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNpi", &Piplus2_MC12TuneV3_ProbNNpi, &b_Piplus2_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNk", &Piplus2_MC12TuneV3_ProbNNk, &b_Piplus2_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNp", &Piplus2_MC12TuneV3_ProbNNp, &b_Piplus2_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Piplus2_MC12TuneV3_ProbNNghost", &Piplus2_MC12TuneV3_ProbNNghost, &b_Piplus2_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNe", &Piplus2_MC12TuneV4_ProbNNe, &b_Piplus2_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNmu", &Piplus2_MC12TuneV4_ProbNNmu, &b_Piplus2_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNpi", &Piplus2_MC12TuneV4_ProbNNpi, &b_Piplus2_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNk", &Piplus2_MC12TuneV4_ProbNNk, &b_Piplus2_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNp", &Piplus2_MC12TuneV4_ProbNNp, &b_Piplus2_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Piplus2_MC12TuneV4_ProbNNghost", &Piplus2_MC12TuneV4_ProbNNghost, &b_Piplus2_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNe", &Piplus2_MC15TuneV1_ProbNNe, &b_Piplus2_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNmu", &Piplus2_MC15TuneV1_ProbNNmu, &b_Piplus2_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNpi", &Piplus2_MC15TuneV1_ProbNNpi, &b_Piplus2_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNk", &Piplus2_MC15TuneV1_ProbNNk, &b_Piplus2_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNp", &Piplus2_MC15TuneV1_ProbNNp, &b_Piplus2_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Piplus2_MC15TuneV1_ProbNNghost", &Piplus2_MC15TuneV1_ProbNNghost, &b_Piplus2_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Piplus2_OWNPV_X", &Piplus2_OWNPV_X, &b_Piplus2_OWNPV_X);
   fChain->SetBranchAddress("Piplus2_OWNPV_Y", &Piplus2_OWNPV_Y, &b_Piplus2_OWNPV_Y);
   fChain->SetBranchAddress("Piplus2_OWNPV_Z", &Piplus2_OWNPV_Z, &b_Piplus2_OWNPV_Z);
   fChain->SetBranchAddress("Piplus2_OWNPV_XERR", &Piplus2_OWNPV_XERR, &b_Piplus2_OWNPV_XERR);
   fChain->SetBranchAddress("Piplus2_OWNPV_YERR", &Piplus2_OWNPV_YERR, &b_Piplus2_OWNPV_YERR);
   fChain->SetBranchAddress("Piplus2_OWNPV_ZERR", &Piplus2_OWNPV_ZERR, &b_Piplus2_OWNPV_ZERR);
   fChain->SetBranchAddress("Piplus2_OWNPV_CHI2", &Piplus2_OWNPV_CHI2, &b_Piplus2_OWNPV_CHI2);
   fChain->SetBranchAddress("Piplus2_OWNPV_NDOF", &Piplus2_OWNPV_NDOF, &b_Piplus2_OWNPV_NDOF);
   fChain->SetBranchAddress("Piplus2_OWNPV_COV_", Piplus2_OWNPV_COV_, &b_Piplus2_OWNPV_COV_);
   fChain->SetBranchAddress("Piplus2_IP_OWNPV", &Piplus2_IP_OWNPV, &b_Piplus2_IP_OWNPV);
   fChain->SetBranchAddress("Piplus2_IPCHI2_OWNPV", &Piplus2_IPCHI2_OWNPV, &b_Piplus2_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Piplus2_ORIVX_X", &Piplus2_ORIVX_X, &b_Piplus2_ORIVX_X);
   fChain->SetBranchAddress("Piplus2_ORIVX_Y", &Piplus2_ORIVX_Y, &b_Piplus2_ORIVX_Y);
   fChain->SetBranchAddress("Piplus2_ORIVX_Z", &Piplus2_ORIVX_Z, &b_Piplus2_ORIVX_Z);
   fChain->SetBranchAddress("Piplus2_ORIVX_XERR", &Piplus2_ORIVX_XERR, &b_Piplus2_ORIVX_XERR);
   fChain->SetBranchAddress("Piplus2_ORIVX_YERR", &Piplus2_ORIVX_YERR, &b_Piplus2_ORIVX_YERR);
   fChain->SetBranchAddress("Piplus2_ORIVX_ZERR", &Piplus2_ORIVX_ZERR, &b_Piplus2_ORIVX_ZERR);
   fChain->SetBranchAddress("Piplus2_ORIVX_CHI2", &Piplus2_ORIVX_CHI2, &b_Piplus2_ORIVX_CHI2);
   fChain->SetBranchAddress("Piplus2_ORIVX_NDOF", &Piplus2_ORIVX_NDOF, &b_Piplus2_ORIVX_NDOF);
   fChain->SetBranchAddress("Piplus2_ORIVX_COV_", Piplus2_ORIVX_COV_, &b_Piplus2_ORIVX_COV_);
   fChain->SetBranchAddress("Piplus2_P", &Piplus2_P, &b_Piplus2_P);
   fChain->SetBranchAddress("Piplus2_PT", &Piplus2_PT, &b_Piplus2_PT);
   fChain->SetBranchAddress("Piplus2_PE", &Piplus2_PE, &b_Piplus2_PE);
   fChain->SetBranchAddress("Piplus2_PX", &Piplus2_PX, &b_Piplus2_PX);
   fChain->SetBranchAddress("Piplus2_PY", &Piplus2_PY, &b_Piplus2_PY);
   fChain->SetBranchAddress("Piplus2_PZ", &Piplus2_PZ, &b_Piplus2_PZ);
   fChain->SetBranchAddress("Piplus2_M", &Piplus2_M, &b_Piplus2_M);
   fChain->SetBranchAddress("Piplus2_ID", &Piplus2_ID, &b_Piplus2_ID);
   fChain->SetBranchAddress("Piplus2_PIDe", &Piplus2_PIDe, &b_Piplus2_PIDe);
   fChain->SetBranchAddress("Piplus2_PIDmu", &Piplus2_PIDmu, &b_Piplus2_PIDmu);
   fChain->SetBranchAddress("Piplus2_PIDK", &Piplus2_PIDK, &b_Piplus2_PIDK);
   fChain->SetBranchAddress("Piplus2_PIDp", &Piplus2_PIDp, &b_Piplus2_PIDp);
   fChain->SetBranchAddress("Piplus2_ProbNNe", &Piplus2_ProbNNe, &b_Piplus2_ProbNNe);
   fChain->SetBranchAddress("Piplus2_ProbNNk", &Piplus2_ProbNNk, &b_Piplus2_ProbNNk);
   fChain->SetBranchAddress("Piplus2_ProbNNp", &Piplus2_ProbNNp, &b_Piplus2_ProbNNp);
   fChain->SetBranchAddress("Piplus2_ProbNNpi", &Piplus2_ProbNNpi, &b_Piplus2_ProbNNpi);
   fChain->SetBranchAddress("Piplus2_ProbNNmu", &Piplus2_ProbNNmu, &b_Piplus2_ProbNNmu);
   fChain->SetBranchAddress("Piplus2_ProbNNghost", &Piplus2_ProbNNghost, &b_Piplus2_ProbNNghost);
   fChain->SetBranchAddress("Piplus2_hasMuon", &Piplus2_hasMuon, &b_Piplus2_hasMuon);
   fChain->SetBranchAddress("Piplus2_isMuon", &Piplus2_isMuon, &b_Piplus2_isMuon);
   fChain->SetBranchAddress("Piplus2_hasRich", &Piplus2_hasRich, &b_Piplus2_hasRich);
   fChain->SetBranchAddress("Piplus2_hasCalo", &Piplus2_hasCalo, &b_Piplus2_hasCalo);
   fChain->SetBranchAddress("Piplus2_TRACK_Type", &Piplus2_TRACK_Type, &b_Piplus2_TRACK_Type);
   fChain->SetBranchAddress("Piplus2_TRACK_Key", &Piplus2_TRACK_Key, &b_Piplus2_TRACK_Key);
   fChain->SetBranchAddress("Piplus2_TRACK_CHI2NDOF", &Piplus2_TRACK_CHI2NDOF, &b_Piplus2_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Piplus2_TRACK_PCHI2", &Piplus2_TRACK_PCHI2, &b_Piplus2_TRACK_PCHI2);
   fChain->SetBranchAddress("Piplus2_TRACK_MatchCHI2", &Piplus2_TRACK_MatchCHI2, &b_Piplus2_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Piplus2_TRACK_GhostProb", &Piplus2_TRACK_GhostProb, &b_Piplus2_TRACK_GhostProb);
   fChain->SetBranchAddress("Piplus2_TRACK_CloneDist", &Piplus2_TRACK_CloneDist, &b_Piplus2_TRACK_CloneDist);
   fChain->SetBranchAddress("Piplus2_TRACK_Likelihood", &Piplus2_TRACK_Likelihood, &b_Piplus2_TRACK_Likelihood);
   fChain->SetBranchAddress("Piplus2_X", &Piplus2_X, &b_Piplus2_X);
   fChain->SetBranchAddress("Piplus2_Y", &Piplus2_Y, &b_Piplus2_Y);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("L0Data_DiMuon_Pt", &L0Data_DiMuon_Pt, &b_L0Data_DiMuon_Pt);
   fChain->SetBranchAddress("L0Data_DiMuonProd_Pt1Pt2", &L0Data_DiMuonProd_Pt1Pt2, &b_L0Data_DiMuonProd_Pt1Pt2);
   fChain->SetBranchAddress("L0Data_Electron_Et", &L0Data_Electron_Et, &b_L0Data_Electron_Et);
   fChain->SetBranchAddress("L0Data_GlobalPi0_Et", &L0Data_GlobalPi0_Et, &b_L0Data_GlobalPi0_Et);
   fChain->SetBranchAddress("L0Data_Hadron_Et", &L0Data_Hadron_Et, &b_L0Data_Hadron_Et);
   fChain->SetBranchAddress("L0Data_LocalPi0_Et", &L0Data_LocalPi0_Et, &b_L0Data_LocalPi0_Et);
   fChain->SetBranchAddress("L0Data_Muon1_Pt", &L0Data_Muon1_Pt, &b_L0Data_Muon1_Pt);
   fChain->SetBranchAddress("L0Data_Muon1_Sgn", &L0Data_Muon1_Sgn, &b_L0Data_Muon1_Sgn);
   fChain->SetBranchAddress("L0Data_Muon2_Pt", &L0Data_Muon2_Pt, &b_L0Data_Muon2_Pt);
   fChain->SetBranchAddress("L0Data_Muon2_Sgn", &L0Data_Muon2_Sgn, &b_L0Data_Muon2_Sgn);
   fChain->SetBranchAddress("L0Data_Muon3_Pt", &L0Data_Muon3_Pt, &b_L0Data_Muon3_Pt);
   fChain->SetBranchAddress("L0Data_Muon3_Sgn", &L0Data_Muon3_Sgn, &b_L0Data_Muon3_Sgn);
   fChain->SetBranchAddress("L0Data_PUHits_Mult", &L0Data_PUHits_Mult, &b_L0Data_PUHits_Mult);
   fChain->SetBranchAddress("L0Data_PUPeak1_Cont", &L0Data_PUPeak1_Cont, &b_L0Data_PUPeak1_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak1_Pos", &L0Data_PUPeak1_Pos, &b_L0Data_PUPeak1_Pos);
   fChain->SetBranchAddress("L0Data_PUPeak2_Cont", &L0Data_PUPeak2_Cont, &b_L0Data_PUPeak2_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak2_Pos", &L0Data_PUPeak2_Pos, &b_L0Data_PUPeak2_Pos);
   fChain->SetBranchAddress("L0Data_Photon_Et", &L0Data_Photon_Et, &b_L0Data_Photon_Et);
   fChain->SetBranchAddress("L0Data_Spd_Mult", &L0Data_Spd_Mult, &b_L0Data_Spd_Mult);
   fChain->SetBranchAddress("L0Data_Sum_Et", &L0Data_Sum_Et, &b_L0Data_Sum_Et);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next1", &L0Data_Sum_Et_Next1, &b_L0Data_Sum_Et_Next1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next2", &L0Data_Sum_Et_Next2, &b_L0Data_Sum_Et_Next2);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev1", &L0Data_Sum_Et_Prev1, &b_L0Data_Sum_Et_Prev1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev2", &L0Data_Sum_Et_Prev2, &b_L0Data_Sum_Et_Prev2);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0HadronDecision", &L0HadronDecision, &b_L0HadronDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1TrackMVADecision", &Hlt1TrackMVADecision, &b_Hlt1TrackMVADecision);
   fChain->SetBranchAddress("Hlt1TwoTrackMVADecision", &Hlt1TwoTrackMVADecision, &b_Hlt1TwoTrackMVADecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   Notify();
}

Bool_t DpToKmPipPip_class::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DpToKmPipPip_class::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DpToKmPipPip_class::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DpToKmPipPip_class_cxx
