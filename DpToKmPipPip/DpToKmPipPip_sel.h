//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct  4 14:51:05 2016 by ROOT version 6.06/01
// from TTree DecayTree/DecayTree
// found on file: TeslaTuples_Sept07A_0x11381609_00_10.root
//////////////////////////////////////////////////////////

#ifndef DpToKmPipPip_sel_h
#define DpToKmPipPip_sel_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector


class DpToKmPipPip_sel : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_X = {fReader, "Dplus_ENDVERTEX_X"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_Y = {fReader, "Dplus_ENDVERTEX_Y"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_Z = {fReader, "Dplus_ENDVERTEX_Z"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_XERR = {fReader, "Dplus_ENDVERTEX_XERR"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_YERR = {fReader, "Dplus_ENDVERTEX_YERR"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_ZERR = {fReader, "Dplus_ENDVERTEX_ZERR"};
   TTreeReaderValue<Double_t> Dplus_ENDVERTEX_CHI2 = {fReader, "Dplus_ENDVERTEX_CHI2"};
   TTreeReaderValue<Int_t> Dplus_ENDVERTEX_NDOF = {fReader, "Dplus_ENDVERTEX_NDOF"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_X = {fReader, "Dplus_OWNPV_X"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_Y = {fReader, "Dplus_OWNPV_Y"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_Z = {fReader, "Dplus_OWNPV_Z"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_XERR = {fReader, "Dplus_OWNPV_XERR"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_YERR = {fReader, "Dplus_OWNPV_YERR"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_ZERR = {fReader, "Dplus_OWNPV_ZERR"};
   TTreeReaderValue<Double_t> Dplus_OWNPV_CHI2 = {fReader, "Dplus_OWNPV_CHI2"};
   TTreeReaderValue<Int_t> Dplus_OWNPV_NDOF = {fReader, "Dplus_OWNPV_NDOF"};
   TTreeReaderValue<Double_t> Dplus_IP_OWNPV = {fReader, "Dplus_IP_OWNPV"};
   TTreeReaderValue<Double_t> Dplus_IPCHI2_OWNPV = {fReader, "Dplus_IPCHI2_OWNPV"};
   TTreeReaderValue<Double_t> Dplus_FD_OWNPV = {fReader, "Dplus_FD_OWNPV"};
   TTreeReaderValue<Double_t> Dplus_FDCHI2_OWNPV = {fReader, "Dplus_FDCHI2_OWNPV"};
   TTreeReaderValue<Double_t> Dplus_DIRA_OWNPV = {fReader, "Dplus_DIRA_OWNPV"};
   TTreeReaderValue<Double_t> Dplus_P = {fReader, "Dplus_P"};
   TTreeReaderValue<Double_t> Dplus_PT = {fReader, "Dplus_PT"};
   TTreeReaderValue<Double_t> Dplus_PE = {fReader, "Dplus_PE"};
   TTreeReaderValue<Double_t> Dplus_PX = {fReader, "Dplus_PX"};
   TTreeReaderValue<Double_t> Dplus_PY = {fReader, "Dplus_PY"};
   TTreeReaderValue<Double_t> Dplus_PZ = {fReader, "Dplus_PZ"};
   TTreeReaderValue<Double_t> Dplus_MM = {fReader, "Dplus_MM"};
   TTreeReaderValue<Double_t> Dplus_MMERR = {fReader, "Dplus_MMERR"};
   TTreeReaderValue<Double_t> Dplus_M = {fReader, "Dplus_M"};
   TTreeReaderValue<Int_t> Dplus_ID = {fReader, "Dplus_ID"};
   TTreeReaderValue<Double_t> Dplus_TAU = {fReader, "Dplus_TAU"};
   TTreeReaderValue<Double_t> Dplus_TAUERR = {fReader, "Dplus_TAUERR"};
   TTreeReaderValue<Double_t> Dplus_TAUCHI2 = {fReader, "Dplus_TAUCHI2"};
   TTreeReaderValue<Double_t> Dplus_X = {fReader, "Dplus_X"};
   TTreeReaderValue<Double_t> Dplus_Y = {fReader, "Dplus_Y"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNe = {fReader, "Kminus_MC12TuneV2_ProbNNe"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNmu = {fReader, "Kminus_MC12TuneV2_ProbNNmu"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNpi = {fReader, "Kminus_MC12TuneV2_ProbNNpi"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNk = {fReader, "Kminus_MC12TuneV2_ProbNNk"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNp = {fReader, "Kminus_MC12TuneV2_ProbNNp"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV2_ProbNNghost = {fReader, "Kminus_MC12TuneV2_ProbNNghost"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNe = {fReader, "Kminus_MC12TuneV3_ProbNNe"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNmu = {fReader, "Kminus_MC12TuneV3_ProbNNmu"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNpi = {fReader, "Kminus_MC12TuneV3_ProbNNpi"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNk = {fReader, "Kminus_MC12TuneV3_ProbNNk"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNp = {fReader, "Kminus_MC12TuneV3_ProbNNp"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV3_ProbNNghost = {fReader, "Kminus_MC12TuneV3_ProbNNghost"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNe = {fReader, "Kminus_MC12TuneV4_ProbNNe"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNmu = {fReader, "Kminus_MC12TuneV4_ProbNNmu"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNpi = {fReader, "Kminus_MC12TuneV4_ProbNNpi"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNk = {fReader, "Kminus_MC12TuneV4_ProbNNk"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNp = {fReader, "Kminus_MC12TuneV4_ProbNNp"};
   TTreeReaderValue<Double_t> Kminus_MC12TuneV4_ProbNNghost = {fReader, "Kminus_MC12TuneV4_ProbNNghost"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNe = {fReader, "Kminus_MC15TuneV1_ProbNNe"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNmu = {fReader, "Kminus_MC15TuneV1_ProbNNmu"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNpi = {fReader, "Kminus_MC15TuneV1_ProbNNpi"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNk = {fReader, "Kminus_MC15TuneV1_ProbNNk"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNp = {fReader, "Kminus_MC15TuneV1_ProbNNp"};
   TTreeReaderValue<Double_t> Kminus_MC15TuneV1_ProbNNghost = {fReader, "Kminus_MC15TuneV1_ProbNNghost"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_X = {fReader, "Kminus_OWNPV_X"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_Y = {fReader, "Kminus_OWNPV_Y"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_Z = {fReader, "Kminus_OWNPV_Z"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_XERR = {fReader, "Kminus_OWNPV_XERR"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_YERR = {fReader, "Kminus_OWNPV_YERR"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_ZERR = {fReader, "Kminus_OWNPV_ZERR"};
   TTreeReaderValue<Double_t> Kminus_OWNPV_CHI2 = {fReader, "Kminus_OWNPV_CHI2"};
   TTreeReaderValue<Int_t> Kminus_OWNPV_NDOF = {fReader, "Kminus_OWNPV_NDOF"};
   TTreeReaderValue<Double_t> Kminus_IP_OWNPV = {fReader, "Kminus_IP_OWNPV"};
   TTreeReaderValue<Double_t> Kminus_IPCHI2_OWNPV = {fReader, "Kminus_IPCHI2_OWNPV"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_X = {fReader, "Kminus_ORIVX_X"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_Y = {fReader, "Kminus_ORIVX_Y"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_Z = {fReader, "Kminus_ORIVX_Z"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_XERR = {fReader, "Kminus_ORIVX_XERR"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_YERR = {fReader, "Kminus_ORIVX_YERR"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_ZERR = {fReader, "Kminus_ORIVX_ZERR"};
   TTreeReaderValue<Double_t> Kminus_ORIVX_CHI2 = {fReader, "Kminus_ORIVX_CHI2"};
   TTreeReaderValue<Int_t> Kminus_ORIVX_NDOF = {fReader, "Kminus_ORIVX_NDOF"};
   TTreeReaderValue<Double_t> Kminus_P = {fReader, "Kminus_P"};
   TTreeReaderValue<Double_t> Kminus_PT = {fReader, "Kminus_PT"};
   TTreeReaderValue<Double_t> Kminus_PE = {fReader, "Kminus_PE"};
   TTreeReaderValue<Double_t> Kminus_PX = {fReader, "Kminus_PX"};
   TTreeReaderValue<Double_t> Kminus_PY = {fReader, "Kminus_PY"};
   TTreeReaderValue<Double_t> Kminus_PZ = {fReader, "Kminus_PZ"};
   TTreeReaderValue<Double_t> Kminus_M = {fReader, "Kminus_M"};
   TTreeReaderValue<Int_t> Kminus_ID = {fReader, "Kminus_ID"};
   TTreeReaderValue<Double_t> Kminus_PIDe = {fReader, "Kminus_PIDe"};
   TTreeReaderValue<Double_t> Kminus_PIDmu = {fReader, "Kminus_PIDmu"};
   TTreeReaderValue<Double_t> Kminus_PIDK = {fReader, "Kminus_PIDK"};
   TTreeReaderValue<Double_t> Kminus_PIDp = {fReader, "Kminus_PIDp"};
   TTreeReaderValue<Double_t> Kminus_ProbNNe = {fReader, "Kminus_ProbNNe"};
   TTreeReaderValue<Double_t> Kminus_ProbNNk = {fReader, "Kminus_ProbNNk"};
   TTreeReaderValue<Double_t> Kminus_ProbNNp = {fReader, "Kminus_ProbNNp"};
   TTreeReaderValue<Double_t> Kminus_ProbNNpi = {fReader, "Kminus_ProbNNpi"};
   TTreeReaderValue<Double_t> Kminus_ProbNNmu = {fReader, "Kminus_ProbNNmu"};
   TTreeReaderValue<Double_t> Kminus_ProbNNghost = {fReader, "Kminus_ProbNNghost"};
   TTreeReaderValue<Bool_t> Kminus_hasMuon = {fReader, "Kminus_hasMuon"};
   TTreeReaderValue<Bool_t> Kminus_isMuon = {fReader, "Kminus_isMuon"};
   TTreeReaderValue<Bool_t> Kminus_hasRich = {fReader, "Kminus_hasRich"};
   TTreeReaderValue<Bool_t> Kminus_hasCalo = {fReader, "Kminus_hasCalo"};
   TTreeReaderValue<Int_t> Kminus_TRACK_Type = {fReader, "Kminus_TRACK_Type"};
   TTreeReaderValue<Int_t> Kminus_TRACK_Key = {fReader, "Kminus_TRACK_Key"};
   TTreeReaderValue<Double_t> Kminus_TRACK_CHI2NDOF = {fReader, "Kminus_TRACK_CHI2NDOF"};
   TTreeReaderValue<Double_t> Kminus_TRACK_PCHI2 = {fReader, "Kminus_TRACK_PCHI2"};
   TTreeReaderValue<Double_t> Kminus_TRACK_MatchCHI2 = {fReader, "Kminus_TRACK_MatchCHI2"};
   TTreeReaderValue<Double_t> Kminus_TRACK_GhostProb = {fReader, "Kminus_TRACK_GhostProb"};
   TTreeReaderValue<Double_t> Kminus_TRACK_CloneDist = {fReader, "Kminus_TRACK_CloneDist"};
   TTreeReaderValue<Double_t> Kminus_TRACK_Likelihood = {fReader, "Kminus_TRACK_Likelihood"};
   TTreeReaderValue<Double_t> Kminus_X = {fReader, "Kminus_X"};
   TTreeReaderValue<Double_t> Kminus_Y = {fReader, "Kminus_Y"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNe = {fReader, "Piplus1_MC12TuneV2_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNmu = {fReader, "Piplus1_MC12TuneV2_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNpi = {fReader, "Piplus1_MC12TuneV2_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNk = {fReader, "Piplus1_MC12TuneV2_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNp = {fReader, "Piplus1_MC12TuneV2_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV2_ProbNNghost = {fReader, "Piplus1_MC12TuneV2_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNe = {fReader, "Piplus1_MC12TuneV3_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNmu = {fReader, "Piplus1_MC12TuneV3_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNpi = {fReader, "Piplus1_MC12TuneV3_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNk = {fReader, "Piplus1_MC12TuneV3_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNp = {fReader, "Piplus1_MC12TuneV3_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV3_ProbNNghost = {fReader, "Piplus1_MC12TuneV3_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNe = {fReader, "Piplus1_MC12TuneV4_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNmu = {fReader, "Piplus1_MC12TuneV4_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNpi = {fReader, "Piplus1_MC12TuneV4_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNk = {fReader, "Piplus1_MC12TuneV4_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNp = {fReader, "Piplus1_MC12TuneV4_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus1_MC12TuneV4_ProbNNghost = {fReader, "Piplus1_MC12TuneV4_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNe = {fReader, "Piplus1_MC15TuneV1_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNmu = {fReader, "Piplus1_MC15TuneV1_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNpi = {fReader, "Piplus1_MC15TuneV1_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNk = {fReader, "Piplus1_MC15TuneV1_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNp = {fReader, "Piplus1_MC15TuneV1_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus1_MC15TuneV1_ProbNNghost = {fReader, "Piplus1_MC15TuneV1_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_X = {fReader, "Piplus1_OWNPV_X"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_Y = {fReader, "Piplus1_OWNPV_Y"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_Z = {fReader, "Piplus1_OWNPV_Z"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_XERR = {fReader, "Piplus1_OWNPV_XERR"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_YERR = {fReader, "Piplus1_OWNPV_YERR"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_ZERR = {fReader, "Piplus1_OWNPV_ZERR"};
   TTreeReaderValue<Double_t> Piplus1_OWNPV_CHI2 = {fReader, "Piplus1_OWNPV_CHI2"};
   TTreeReaderValue<Int_t> Piplus1_OWNPV_NDOF = {fReader, "Piplus1_OWNPV_NDOF"};
   TTreeReaderValue<Double_t> Piplus1_IP_OWNPV = {fReader, "Piplus1_IP_OWNPV"};
   TTreeReaderValue<Double_t> Piplus1_IPCHI2_OWNPV = {fReader, "Piplus1_IPCHI2_OWNPV"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_X = {fReader, "Piplus1_ORIVX_X"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_Y = {fReader, "Piplus1_ORIVX_Y"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_Z = {fReader, "Piplus1_ORIVX_Z"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_XERR = {fReader, "Piplus1_ORIVX_XERR"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_YERR = {fReader, "Piplus1_ORIVX_YERR"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_ZERR = {fReader, "Piplus1_ORIVX_ZERR"};
   TTreeReaderValue<Double_t> Piplus1_ORIVX_CHI2 = {fReader, "Piplus1_ORIVX_CHI2"};
   TTreeReaderValue<Int_t> Piplus1_ORIVX_NDOF = {fReader, "Piplus1_ORIVX_NDOF"};
   TTreeReaderValue<Double_t> Piplus1_P = {fReader, "Piplus1_P"};
   TTreeReaderValue<Double_t> Piplus1_PT = {fReader, "Piplus1_PT"};
   TTreeReaderValue<Double_t> Piplus1_PE = {fReader, "Piplus1_PE"};
   TTreeReaderValue<Double_t> Piplus1_PX = {fReader, "Piplus1_PX"};
   TTreeReaderValue<Double_t> Piplus1_PY = {fReader, "Piplus1_PY"};
   TTreeReaderValue<Double_t> Piplus1_PZ = {fReader, "Piplus1_PZ"};
   TTreeReaderValue<Double_t> Piplus1_M = {fReader, "Piplus1_M"};
   TTreeReaderValue<Int_t> Piplus1_ID = {fReader, "Piplus1_ID"};
   TTreeReaderValue<Double_t> Piplus1_PIDe = {fReader, "Piplus1_PIDe"};
   TTreeReaderValue<Double_t> Piplus1_PIDmu = {fReader, "Piplus1_PIDmu"};
   TTreeReaderValue<Double_t> Piplus1_PIDK = {fReader, "Piplus1_PIDK"};
   TTreeReaderValue<Double_t> Piplus1_PIDp = {fReader, "Piplus1_PIDp"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNe = {fReader, "Piplus1_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNk = {fReader, "Piplus1_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNp = {fReader, "Piplus1_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNpi = {fReader, "Piplus1_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNmu = {fReader, "Piplus1_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus1_ProbNNghost = {fReader, "Piplus1_ProbNNghost"};
   TTreeReaderValue<Bool_t> Piplus1_hasMuon = {fReader, "Piplus1_hasMuon"};
   TTreeReaderValue<Bool_t> Piplus1_isMuon = {fReader, "Piplus1_isMuon"};
   TTreeReaderValue<Bool_t> Piplus1_hasRich = {fReader, "Piplus1_hasRich"};
   TTreeReaderValue<Bool_t> Piplus1_hasCalo = {fReader, "Piplus1_hasCalo"};
   TTreeReaderValue<Int_t> Piplus1_TRACK_Type = {fReader, "Piplus1_TRACK_Type"};
   TTreeReaderValue<Int_t> Piplus1_TRACK_Key = {fReader, "Piplus1_TRACK_Key"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_CHI2NDOF = {fReader, "Piplus1_TRACK_CHI2NDOF"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_PCHI2 = {fReader, "Piplus1_TRACK_PCHI2"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_MatchCHI2 = {fReader, "Piplus1_TRACK_MatchCHI2"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_GhostProb = {fReader, "Piplus1_TRACK_GhostProb"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_CloneDist = {fReader, "Piplus1_TRACK_CloneDist"};
   TTreeReaderValue<Double_t> Piplus1_TRACK_Likelihood = {fReader, "Piplus1_TRACK_Likelihood"};
   TTreeReaderValue<Double_t> Piplus1_X = {fReader, "Piplus1_X"};
   TTreeReaderValue<Double_t> Piplus1_Y = {fReader, "Piplus1_Y"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNe = {fReader, "Piplus2_MC12TuneV2_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNmu = {fReader, "Piplus2_MC12TuneV2_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNpi = {fReader, "Piplus2_MC12TuneV2_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNk = {fReader, "Piplus2_MC12TuneV2_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNp = {fReader, "Piplus2_MC12TuneV2_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV2_ProbNNghost = {fReader, "Piplus2_MC12TuneV2_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNe = {fReader, "Piplus2_MC12TuneV3_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNmu = {fReader, "Piplus2_MC12TuneV3_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNpi = {fReader, "Piplus2_MC12TuneV3_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNk = {fReader, "Piplus2_MC12TuneV3_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNp = {fReader, "Piplus2_MC12TuneV3_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV3_ProbNNghost = {fReader, "Piplus2_MC12TuneV3_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNe = {fReader, "Piplus2_MC12TuneV4_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNmu = {fReader, "Piplus2_MC12TuneV4_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNpi = {fReader, "Piplus2_MC12TuneV4_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNk = {fReader, "Piplus2_MC12TuneV4_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNp = {fReader, "Piplus2_MC12TuneV4_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus2_MC12TuneV4_ProbNNghost = {fReader, "Piplus2_MC12TuneV4_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNe = {fReader, "Piplus2_MC15TuneV1_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNmu = {fReader, "Piplus2_MC15TuneV1_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNpi = {fReader, "Piplus2_MC15TuneV1_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNk = {fReader, "Piplus2_MC15TuneV1_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNp = {fReader, "Piplus2_MC15TuneV1_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus2_MC15TuneV1_ProbNNghost = {fReader, "Piplus2_MC15TuneV1_ProbNNghost"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_X = {fReader, "Piplus2_OWNPV_X"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_Y = {fReader, "Piplus2_OWNPV_Y"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_Z = {fReader, "Piplus2_OWNPV_Z"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_XERR = {fReader, "Piplus2_OWNPV_XERR"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_YERR = {fReader, "Piplus2_OWNPV_YERR"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_ZERR = {fReader, "Piplus2_OWNPV_ZERR"};
   TTreeReaderValue<Double_t> Piplus2_OWNPV_CHI2 = {fReader, "Piplus2_OWNPV_CHI2"};
   TTreeReaderValue<Int_t> Piplus2_OWNPV_NDOF = {fReader, "Piplus2_OWNPV_NDOF"};
   TTreeReaderValue<Double_t> Piplus2_IP_OWNPV = {fReader, "Piplus2_IP_OWNPV"};
   TTreeReaderValue<Double_t> Piplus2_IPCHI2_OWNPV = {fReader, "Piplus2_IPCHI2_OWNPV"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_X = {fReader, "Piplus2_ORIVX_X"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_Y = {fReader, "Piplus2_ORIVX_Y"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_Z = {fReader, "Piplus2_ORIVX_Z"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_XERR = {fReader, "Piplus2_ORIVX_XERR"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_YERR = {fReader, "Piplus2_ORIVX_YERR"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_ZERR = {fReader, "Piplus2_ORIVX_ZERR"};
   TTreeReaderValue<Double_t> Piplus2_ORIVX_CHI2 = {fReader, "Piplus2_ORIVX_CHI2"};
   TTreeReaderValue<Int_t> Piplus2_ORIVX_NDOF = {fReader, "Piplus2_ORIVX_NDOF"};
   TTreeReaderValue<Double_t> Piplus2_P = {fReader, "Piplus2_P"};
   TTreeReaderValue<Double_t> Piplus2_PT = {fReader, "Piplus2_PT"};
   TTreeReaderValue<Double_t> Piplus2_PE = {fReader, "Piplus2_PE"};
   TTreeReaderValue<Double_t> Piplus2_PX = {fReader, "Piplus2_PX"};
   TTreeReaderValue<Double_t> Piplus2_PY = {fReader, "Piplus2_PY"};
   TTreeReaderValue<Double_t> Piplus2_PZ = {fReader, "Piplus2_PZ"};
   TTreeReaderValue<Double_t> Piplus2_M = {fReader, "Piplus2_M"};
   TTreeReaderValue<Int_t> Piplus2_ID = {fReader, "Piplus2_ID"};
   TTreeReaderValue<Double_t> Piplus2_PIDe = {fReader, "Piplus2_PIDe"};
   TTreeReaderValue<Double_t> Piplus2_PIDmu = {fReader, "Piplus2_PIDmu"};
   TTreeReaderValue<Double_t> Piplus2_PIDK = {fReader, "Piplus2_PIDK"};
   TTreeReaderValue<Double_t> Piplus2_PIDp = {fReader, "Piplus2_PIDp"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNe = {fReader, "Piplus2_ProbNNe"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNk = {fReader, "Piplus2_ProbNNk"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNp = {fReader, "Piplus2_ProbNNp"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNpi = {fReader, "Piplus2_ProbNNpi"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNmu = {fReader, "Piplus2_ProbNNmu"};
   TTreeReaderValue<Double_t> Piplus2_ProbNNghost = {fReader, "Piplus2_ProbNNghost"};
   TTreeReaderValue<Bool_t> Piplus2_hasMuon = {fReader, "Piplus2_hasMuon"};
   TTreeReaderValue<Bool_t> Piplus2_isMuon = {fReader, "Piplus2_isMuon"};
   TTreeReaderValue<Bool_t> Piplus2_hasRich = {fReader, "Piplus2_hasRich"};
   TTreeReaderValue<Bool_t> Piplus2_hasCalo = {fReader, "Piplus2_hasCalo"};
   TTreeReaderValue<Int_t> Piplus2_TRACK_Type = {fReader, "Piplus2_TRACK_Type"};
   TTreeReaderValue<Int_t> Piplus2_TRACK_Key = {fReader, "Piplus2_TRACK_Key"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_CHI2NDOF = {fReader, "Piplus2_TRACK_CHI2NDOF"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_PCHI2 = {fReader, "Piplus2_TRACK_PCHI2"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_MatchCHI2 = {fReader, "Piplus2_TRACK_MatchCHI2"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_GhostProb = {fReader, "Piplus2_TRACK_GhostProb"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_CloneDist = {fReader, "Piplus2_TRACK_CloneDist"};
   TTreeReaderValue<Double_t> Piplus2_TRACK_Likelihood = {fReader, "Piplus2_TRACK_Likelihood"};
   TTreeReaderValue<Double_t> Piplus2_X = {fReader, "Piplus2_X"};
   TTreeReaderValue<Double_t> Piplus2_Y = {fReader, "Piplus2_Y"};
   TTreeReaderValue<UInt_t> nCandidate = {fReader, "nCandidate"};
   TTreeReaderValue<ULong64_t> totCandidates = {fReader, "totCandidates"};
   TTreeReaderValue<ULong64_t> EventInSequence = {fReader, "EventInSequence"};
   TTreeReaderValue<UInt_t> runNumber = {fReader, "runNumber"};
   TTreeReaderValue<ULong64_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<UInt_t> BCID = {fReader, "BCID"};
   TTreeReaderValue<Int_t> BCType = {fReader, "BCType"};
   TTreeReaderValue<UInt_t> OdinTCK = {fReader, "OdinTCK"};
   TTreeReaderValue<UInt_t> L0DUTCK = {fReader, "L0DUTCK"};
   TTreeReaderValue<UInt_t> HLT1TCK = {fReader, "HLT1TCK"};
   TTreeReaderValue<UInt_t> HLT2TCK = {fReader, "HLT2TCK"};
   TTreeReaderValue<ULong64_t> GpsTime = {fReader, "GpsTime"};
   TTreeReaderValue<Short_t> Polarity = {fReader, "Polarity"};
   TTreeReaderValue<Int_t> L0Data_DiMuon_Pt = {fReader, "L0Data_DiMuon_Pt"};
   TTreeReaderValue<Int_t> L0Data_DiMuonProd_Pt1Pt2 = {fReader, "L0Data_DiMuonProd_Pt1Pt2"};
   TTreeReaderValue<Int_t> L0Data_Electron_Et = {fReader, "L0Data_Electron_Et"};
   TTreeReaderValue<Int_t> L0Data_GlobalPi0_Et = {fReader, "L0Data_GlobalPi0_Et"};
   TTreeReaderValue<Int_t> L0Data_Hadron_Et = {fReader, "L0Data_Hadron_Et"};
   TTreeReaderValue<Int_t> L0Data_LocalPi0_Et = {fReader, "L0Data_LocalPi0_Et"};
   TTreeReaderValue<Int_t> L0Data_Muon1_Pt = {fReader, "L0Data_Muon1_Pt"};
   TTreeReaderValue<Int_t> L0Data_Muon1_Sgn = {fReader, "L0Data_Muon1_Sgn"};
   TTreeReaderValue<Int_t> L0Data_Muon2_Pt = {fReader, "L0Data_Muon2_Pt"};
   TTreeReaderValue<Int_t> L0Data_Muon2_Sgn = {fReader, "L0Data_Muon2_Sgn"};
   TTreeReaderValue<Int_t> L0Data_Muon3_Pt = {fReader, "L0Data_Muon3_Pt"};
   TTreeReaderValue<Int_t> L0Data_Muon3_Sgn = {fReader, "L0Data_Muon3_Sgn"};
   TTreeReaderValue<Int_t> L0Data_PUHits_Mult = {fReader, "L0Data_PUHits_Mult"};
   TTreeReaderValue<Int_t> L0Data_PUPeak1_Cont = {fReader, "L0Data_PUPeak1_Cont"};
   TTreeReaderValue<Int_t> L0Data_PUPeak1_Pos = {fReader, "L0Data_PUPeak1_Pos"};
   TTreeReaderValue<Int_t> L0Data_PUPeak2_Cont = {fReader, "L0Data_PUPeak2_Cont"};
   TTreeReaderValue<Int_t> L0Data_PUPeak2_Pos = {fReader, "L0Data_PUPeak2_Pos"};
   TTreeReaderValue<Int_t> L0Data_Photon_Et = {fReader, "L0Data_Photon_Et"};
   TTreeReaderValue<Int_t> L0Data_Spd_Mult = {fReader, "L0Data_Spd_Mult"};
   TTreeReaderValue<Int_t> L0Data_Sum_Et = {fReader, "L0Data_Sum_Et"};
   TTreeReaderValue<Int_t> L0Data_Sum_Et,Next1 = {fReader, "L0Data_Sum_Et,Next1"};
   TTreeReaderValue<Int_t> L0Data_Sum_Et,Next2 = {fReader, "L0Data_Sum_Et,Next2"};
   TTreeReaderValue<Int_t> L0Data_Sum_Et,Prev1 = {fReader, "L0Data_Sum_Et,Prev1"};
   TTreeReaderValue<Int_t> L0Data_Sum_Et,Prev2 = {fReader, "L0Data_Sum_Et,Prev2"};
   TTreeReaderValue<Int_t> nPV = {fReader, "nPV"};
   TTreeReaderArray<Float_t> PVX = {fReader, "PVX"};
   TTreeReaderArray<Float_t> PVY = {fReader, "PVY"};
   TTreeReaderArray<Float_t> PVZ = {fReader, "PVZ"};
   TTreeReaderArray<Float_t> PVXERR = {fReader, "PVXERR"};
   TTreeReaderArray<Float_t> PVYERR = {fReader, "PVYERR"};
   TTreeReaderArray<Float_t> PVZERR = {fReader, "PVZERR"};
   TTreeReaderArray<Float_t> PVCHI2 = {fReader, "PVCHI2"};
   TTreeReaderArray<Float_t> PVNDOF = {fReader, "PVNDOF"};
   TTreeReaderArray<Float_t> PVNTRACKS = {fReader, "PVNTRACKS"};
   TTreeReaderValue<Int_t> nPVs = {fReader, "nPVs"};
   TTreeReaderValue<Int_t> nTracks = {fReader, "nTracks"};
   TTreeReaderValue<Int_t> nLongTracks = {fReader, "nLongTracks"};
   TTreeReaderValue<Int_t> nDownstreamTracks = {fReader, "nDownstreamTracks"};
   TTreeReaderValue<Int_t> nUpstreamTracks = {fReader, "nUpstreamTracks"};
   TTreeReaderValue<Int_t> nVeloTracks = {fReader, "nVeloTracks"};
   TTreeReaderValue<Int_t> nTTracks = {fReader, "nTTracks"};
   TTreeReaderValue<Int_t> nBackTracks = {fReader, "nBackTracks"};
   TTreeReaderValue<Int_t> nRich1Hits = {fReader, "nRich1Hits"};
   TTreeReaderValue<Int_t> nRich2Hits = {fReader, "nRich2Hits"};
   TTreeReaderValue<Int_t> nVeloClusters = {fReader, "nVeloClusters"};
   TTreeReaderValue<Int_t> nITClusters = {fReader, "nITClusters"};
   TTreeReaderValue<Int_t> nTTClusters = {fReader, "nTTClusters"};
   TTreeReaderValue<Int_t> nOTClusters = {fReader, "nOTClusters"};
   TTreeReaderValue<Int_t> nSPDHits = {fReader, "nSPDHits"};
   TTreeReaderValue<Int_t> nMuonCoordsS0 = {fReader, "nMuonCoordsS0"};
   TTreeReaderValue<Int_t> nMuonCoordsS1 = {fReader, "nMuonCoordsS1"};
   TTreeReaderValue<Int_t> nMuonCoordsS2 = {fReader, "nMuonCoordsS2"};
   TTreeReaderValue<Int_t> nMuonCoordsS3 = {fReader, "nMuonCoordsS3"};
   TTreeReaderValue<Int_t> nMuonCoordsS4 = {fReader, "nMuonCoordsS4"};
   TTreeReaderValue<Int_t> nMuonTracks = {fReader, "nMuonTracks"};
   TTreeReaderValue<Int_t> L0Global = {fReader, "L0Global"};
   TTreeReaderValue<UInt_t> Hlt1Global = {fReader, "Hlt1Global"};
   TTreeReaderValue<UInt_t> Hlt2Global = {fReader, "Hlt2Global"};
   TTreeReaderValue<Int_t> L0HadronDecision = {fReader, "L0HadronDecision"};
   TTreeReaderValue<UInt_t> L0nSelections = {fReader, "L0nSelections"};
   TTreeReaderValue<Int_t> Hlt1TrackMVADecision = {fReader, "Hlt1TrackMVADecision"};
   TTreeReaderValue<Int_t> Hlt1TwoTrackMVADecision = {fReader, "Hlt1TwoTrackMVADecision"};
   TTreeReaderValue<UInt_t> Hlt1nSelections = {fReader, "Hlt1nSelections"};
   TTreeReaderValue<Int_t> MaxRoutingBits = {fReader, "MaxRoutingBits"};
   TTreeReaderArray<Float_t> RoutingBits = {fReader, "RoutingBits"};


   DpToKmPipPip_sel(TTree * /*tree*/ =0) { }
   virtual ~DpToKmPipPip_sel() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(DpToKmPipPip_sel,0);

};

#endif

#ifdef DpToKmPipPip_sel_cxx
void DpToKmPipPip_sel::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t DpToKmPipPip_sel::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef DpToKmPipPip_sel_cxx
